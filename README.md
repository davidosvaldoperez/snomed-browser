# Para los imports de datos en mongo

# Desde fuera del container
docker-compose exec mongo mongorestore /dump/esedition_20171031/
docker-compose exec mongo mongorestore /dump/resourcesCollection/

# Desde dentro del container
docker-compose exec mongo bash
$ cd /dump
$ mongorestore esedition_20171031/
$ mongorestore resourcesCollection/
$ exit
